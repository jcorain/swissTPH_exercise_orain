# Introduction

This folder contains the p_reduct.R file and its test asked for the exercise 4 of the swissTPH exercises for Jean-Christophe Orain.

# How to run the test

The tests are in the folder tests/testthat and in the file test-p_reduct.R. To run them simply use within R environment´
```
testthat::test_dir("the_path_to_tests/testthat")
```